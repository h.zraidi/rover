import com.nasa.RoverPiloter;
import com.nasa.model.Location;
import com.nasa.model.Rover;
import com.nasa.utils.Command;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRover {
    RoverPiloter piloter ;
    Rover rover;

    @Before
    public void initRover() {
        this.piloter = new RoverPiloter();
        this.rover = new Rover();
        piloter.setBoundsX(5);
        piloter.setBoundsY(5);
        rover.setLocation(Location.North);
    }

    @Test
    public void testCommandM() {
        piloter.execute(Command.R, rover);
        assert (Location.East == rover.getLocation());
        int oldY = rover.getY();
        int oldX = rover.getX();
        piloter.execute(Command.M, rover);
        assertEquals(rover.getX(), ++oldX);
        piloter.execute(Command.L, rover);
        assert (Location.North == rover.getLocation());
        piloter.execute(Command.M, rover);
        assertEquals(rover.getY(), ++oldY);
        piloter.execute(Command.M, rover);
        assertEquals(rover.getY(), ++oldY);
    }

    @Test
    public void testCommandR() {
        piloter.reset(rover);
        piloter.execute(Command.R, rover);
        assertEquals(Location.East, rover.getLocation());
    }

    @Test
    public void testCommandL() {
        piloter.reset(rover);
        piloter.execute(Command.L, rover);
        assertEquals(Location.West, rover.getLocation());
    }


    //   1 2 N
    //   LMLMLMLMM
    // R = 1 3 N
    @Test
    public void testInput() {
        piloter.setBoundsY(5);
        piloter.setBoundsX(5);
        piloter.initPosition(1, 2, Location.North, rover);
        piloter.execute(Command.L, rover);
        piloter.execute(Command.M, rover);
        piloter.execute(Command.L, rover);
        piloter.execute(Command.M, rover);
        piloter.execute(Command.L, rover);
        piloter.execute(Command.M, rover);
        piloter.execute(Command.L, rover);
        piloter.execute(Command.M, rover);
        piloter.execute(Command.M, rover);
        assert (Location.North == rover.getLocation());
        assertEquals(rover.getX(), 1);
        assertEquals(rover.getY(), 3);

    }

}
