package com.nasa.model;

public enum Location {
    North, South, East, West;

    public static Location getLocation(char symbol) {
        Location retour = null;
        switch (symbol) {
            case 'N':
                retour = Location.North;
                break;
            case 'S':
                retour = Location.South;
                break;
            case 'E':
                retour = Location.East;
                break;
            case 'W':
                retour = Location.West;
                break;
            default:
                break;
        }
        return retour;
    }


    public char getCode() {
        char retour = 0;
        if (this == Location.North) {
            retour = 'N';
        } else if (this == Location.East) {
            retour = 'E';
        } else if (this == Location.West) {
            retour = 'W';
        } else if (this == Location.South) {
            retour = 'S';
        }
        return retour;
    }
}
