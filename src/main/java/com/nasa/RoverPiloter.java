package com.nasa;

import com.nasa.utils.Command;
import com.nasa.model.Location;
import com.nasa.model.Rover;
import org.apache.log4j.Logger;


public class RoverPiloter {
    private final Logger logger = Logger.getLogger(RoverPiloter.class);
    private int boundsX;
    private int boundsY;

    public void execute(Command command, Rover rover) {
        Location location = rover.getLocation();
        if (command == Command.R) {
            if (location == Location.North) {
                rover.setLocation(Location.East);
            } else if (location == Location.South) {
                rover.setLocation(Location.West);
            } else if (location == Location.East) {
                rover.setLocation(Location.South);
            } else {
                rover.setLocation(Location.North);
            }
        } else if (command == Command.L) {
            if (location == Location.North) {
                rover.setLocation(Location.West);
            } else if (location == Location.South) {
                rover.setLocation(Location.East);
            } else if (location == Location.East) {
                rover.setLocation(Location.North);
            } else {
                rover.setLocation(Location.South);
            }
        } else if (command == Command.M) {
            if (rover.getLocation() == Location.North) {
                moveOneStepY(rover, 1);
            } else if (rover.getLocation() == Location.East) {
                moveOneStepX(rover, +1);
            } else if (rover.getLocation() == Location.West) {
                moveOneStepX(rover, -1);
            } else if (rover.getLocation() == Location.South) {
                moveOneStepY(rover, -1);
            }


        }
    }

    private void moveOneStepX(Rover rover, int val) {
        if (val > 0 && rover.getX() < this.boundsX) {
            rover.setX((rover.getX() + val));
        } else if (val < 0 && rover.getX() > 0) {
            rover.setX((rover.getX() + val ));
        } else {
            logger.warn("cannot execute the command , over range of values found " + (rover.getX() + val) + " min val =0 max value " + getBoundsX());
        }
    }

    private void moveOneStepY(Rover rover, int val) {
        if (val > 0 && rover.getY() < this.boundsY) {
            rover.setY((rover.getY() + val));
        } else if (val < 0 && rover.getY() > 0) {
            rover.setY((rover.getY() + val));
        } else {
            logger.warn("cannot execute the command , over range of values found " + (rover.getY() + val) + " min val =0 max value " + getBoundsY());
        }
    }

    public void initPosition(int x, int y, Location location, Rover rover) {
        rover.setY(y);
        rover.setX(x);
        rover.setLocation(location);
    }

    public void reset(Rover rover) {
        rover.setLocation(Location.North);
        rover.setX(0);
        rover.setY(0);
    }

    public void setBoundsX(int boundsX) {
        this.boundsX = boundsX;
    }

    public int getBoundsX() {
        return boundsX;
    }

    public int getBoundsY() {
        return boundsY;
    }

    public void setBoundsY(int boundsY) {
        this.boundsY = boundsY;
    }
}
