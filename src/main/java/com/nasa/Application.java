package com.nasa;

import com.nasa.model.Location;
import com.nasa.model.Rover;
import com.nasa.utils.Command;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Application {
    private static final Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {

        String fileName = args[0];
        Rover rover = new Rover();
        RoverPiloter piloter = new RoverPiloter();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(line -> {
                        if (line.matches("\\d\\s\\d\\s\\w")) {
                            int x = Integer.parseInt(line.split("\\ ")[0]);
                            int y = Integer.parseInt(line.split("\\ ")[1]);
                            Location location = Location.getLocation(line.split("\\ ")[2].charAt(0));
                            piloter.initPosition(x, y, location, rover);
                        } else if (line.matches("\\d\\s\\d")) {
                            int top = Integer.parseInt(line.split("\\ ")[0]);
                            int right = Integer.parseInt(line.split("\\ ")[1]);
                            logger.info("Initialized the position of rover x =" + top + " right =" + right);
                            piloter.setBoundsY(right);
                            piloter.setBoundsX(top);

                        } else {
                            for (char c : line.toCharArray()) {
                                piloter.execute(Command.getCommand(c), rover);
                            }
                            logger.info(rover.toString());
                        }
                    }

            );

        } catch (IOException e) {
            logger.error(e);
        }

    }


}
