package com.nasa.utils;

public enum Command {
    R, L, M;

    public static Command getCommand(char cmd) {
        Command retour = null;
        switch (cmd) {
            case 'R':
                retour = Command.R;
                break;

            case 'L':
                retour = Command.L;
                break;

            case 'M':
                retour = Command.M;
                break;
            default:
                break;

        }
        return retour;
    }
}